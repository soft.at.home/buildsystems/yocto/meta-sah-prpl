FILES_${PN} += "${libdir}/libnl-3${SOLIBS} \
               ${libdir}/libnl${SOLIBS}"
FILES_${PN}-cli   += "${libdir}/libnl-cli-3${SOLIBS}"
FILES_${PN}-genl  += "${libdir}/libnl-genl-3${SOLIBS} \
                     ${libdir}/libnl-genl${SOLIBS}"
FILES_${PN}-idiag += "${libdir}/libnl-idiag-3${SOLIBS}"
FILES_${PN}-nf    += "${libdir}/libnl-nf-3${SOLIBS}"
FILES_${PN}-route += "${libdir}/libnl-route-3${SOLIBS}"
FILES_${PN}-xfrm  += "${libdir}/libnl-xfrm-3${SOLIBS}"
