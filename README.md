# meta-sah-prpl

Distro layer containing configuration for building prpl compatible packages.

## Adding the meta-sah-prpl layer to your build

```bash
bitbake-layers add-layer meta-sah-prpl
```

## Layer content

### Distro
#### sah_prpl distr
The `sah_prpl` distro provides configuration to match the libc and linux-libc-headers settings to the buildsystem used for the prplWrt builds.
The `SOLIBS` and `SOLIBSDEV` variables are also tweaked to allow the packaging of `.so` symlinks (used for versioning the shared objects) in the released packages.

### Classes
#### package_prpl
This class is a fork of the default `ipk` packaging class in Yocto.
This implementation will use a modified compression scheme to match what's expected by the opkg installation on the prpl build.
It is also possible to overwrite the packaged Architecture field (as this should also be an exact match).
Finally, the dependency on the used libc is also stripped from the package, as this is assumed to be already present on the board and not installed with a package.

### Recipes
- linux-libc-headers: Version `4.14.169` of the linux-libc-headers match the ones used in prplWrt, but were unfortunately no longer available in the used release of Yocto. As such, they have been ported over to this Yocto version.

### bbappends
- libnemo: NeMo defaults that match the target platform `netgear nighthawk X4S`.
- ncurses: Modify default packaging behavior to allow compilation
- pcb-bus: Modify default startup order to better fit with default prpl system startup order
- util-linux: Modify default packaging behavior to allow compilation
- libnl: Modify default packaging behavior to allow compilation

### Configurations

- To use these templates for x86-64 compilations when setting up a build environment:
```bash
    export TEMPLATECONF=meta-sah/tools/meta-sah-prpl/conf/genericx86-64/
    source oe-init-build-env
```

- To use these templates for the `cortex a15` when setting up a build environment:
```bash
    export TEMPLATECONF=meta-sah/tools/meta-sah-prpl/conf/cortexa15-neon-vfpv4/
    source oe-init-build-env
```
