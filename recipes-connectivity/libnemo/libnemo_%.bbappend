FILESEXTRAPATHS_prepend := "${THISDIR}/files:"

SRC_URI += "file://00_nemo-defaults.odl"

do_install_append(){
    install -D -m 644 ${WORKDIR}/00_nemo-defaults.odl ${D}${CONFIG_SAH_LIB_NEMO_DEFAULTS_PATH}/00_nemo-defaults.odl
}
